# CasperJS IDE Helper #

This is an IDE Helper developed by [Christopher Sax](http://christophersax.com).

## How to use it ##

Just download the file and include it in your project. Please mind that the file only contains definitions, not the functions itself and should therefore not be included in production.

## Variable definitions ##

The IDE Helper utilizes variables as objects to trigger the autocompletion. There is one variable per module:

* var **casper** - The casper module

* var **__utils__** - The clientutils module

* var **colorizer** - The colorizer module

* var **mouse** - The mouse module

* var **test** - The tester module

* var **utils** - The utils module

## Issues ##
If you happen to have suggestions or find a bug, please use the issue tracker here on Bitbucket. Thanks!